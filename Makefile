swagger:
	docker run  -p 80:8080  -v $(pwd)/public/swagger:/tmp -e SWAGGER_FILE=/tmp/swagger.json  swaggerapi/swagger-editor
test:
	./vendor/bin/phpunit --colors=auto

start:
	symfony server:start

migrate:
	bin/console doctrine:migrations:migrate

newmigrate:
	bin/console doctrine:migrations:generate

cli:
	wget https://get.symfony.com/cli/installer -O - | bash
	
symfony:
	wget https://get.symfony.com/cli/installer -O - | bash
	sudo mv /home/${USER}/.symfony/bin/symfony /usr/local/bin/symfony

php-ext:
	sudo apt install php7.4 php7.4-fpm php7.4-common php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring php7.4-opcache php7.4-soap php7.4-zip php7.4-intl php7.4-bcmath unzip -y
