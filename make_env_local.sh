#!/bin/bash

#check variables exist
hasError="false"
if [ -z ${DB_PORT+x} ]
then echo "env variable DB_PORT not set. Error"; hasError="true"
fi

if [ -z ${DB_HOST+x} ]
then echo "env variable DB_PORT not set. Error"; hasError="true"
fi

if [ -z ${DB_NAME+x} ] 
then echo "env variable DB_NAME not set. Error"; hasError="true"
fi

if [ -z ${DB_USER+x} ] 
then echo "env variable DB_USER not set. Error"; hasError="true"
fi

if [ -z ${DB_SECRET+x} ] 
then echo "env variable DB_SECRET not set. Error"; hasError="true"
fi

if [ -z ${DB_VERSION+x} ]
then echo "env variable DB_VERSION not set. Error"; hasError="true"
fi

if [ -z ${APP_SECRET+x} ] 
then echo "env variable APP_SECRET not set. Error"; hasError="true"
fi

if [ -z ${JWT_PASSPHRASE+x} ] 
then echo "env variable JWT_PASSPHRASE not set. Error"; hasError="true"
fi

if [ $hasError=="true" ]
then echo $hasError
#exit 1
fi

echo "" > .env.local
echo "APP_ENV=prod" >> .env.local
echo "DB_PORT=${DB_PORT}" >> .env.local
echo "DB_HOST=${DB_HOST}" >> .env.local
echo "DB_NAME=${DB_NAME}" >> .env.local
echo "DB_USER=${DB_USER}" >> .env.local
echo "DB_SECRET=${DB_SECRET}" >> .env.local
echo "DB_VERSION=${DB_VERSION}" >> .env.local
echo 'DATABASE_URL="mysql://${DB_USER}:${DB_SECRET}@${DB_HOST}:${DB_PORT}/${DB_NAME}?serverVersion=${DB_VERSION}"' >> .env.local
echo "APP_SECRET=${APP_SECRET}" >> .env.local
echo "JWT_PASSPHRASE=${JWT_PASSPHRASE}" >> .env.local
echo "env.local created"
